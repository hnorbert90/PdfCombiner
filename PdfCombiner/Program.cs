﻿using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;

namespace PDFCombiner
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] pdfFiles = ListPdfFiles(args[0]);

            CombinePdfFiles(pdfFiles, args[1]);
        }

        static void CombinePdfFiles(string[] filePaths, string outputPath)
        {
            // Create a new PDF document
            var document = new PdfDocument(new PdfWriter(new FileStream(outputPath, FileMode.Create)));

            // Set up a timer to update the progress bar
            var timer = new System.Timers.Timer(100);
            timer.Elapsed += (sender, e) => UpdateProgressBar(filePaths.Length);
            timer.Start();

            // Set up variables to track progress
            int progress = 0;
            int progressInterval = (int)Math.Ceiling(filePaths.Length / 50.0);

            // Loop through the list of PDF file paths
            foreach (string filePath in filePaths)
            {
                // Check if the file exists
                if (File.Exists(filePath))
                {
                    // Open the PDF file
                    var reader = new PdfReader(filePath);
                    var pdf = new PdfDocument(reader);
                    var pageCount = pdf.GetNumberOfPages();

                    // Loop through all the pages in the file
                    for (int i = 1; i <= pageCount; i++)
                    {
                        // Get the page from the original document
                        PdfPage page = pdf.GetPage(i);
                        // Get the page size of the original page
                        Rectangle pageSize = page.GetPageSize();
                        int rotation = page.GetRotation();
                        // Create a new page for the new document
                        PdfPage newPage = document.AddNewPage(new PageSize(pageSize));
                        newPage.SetRotation(rotation);
                        // Get a canvas for the new page
                        PdfCanvas canvas = new PdfCanvas(newPage);
                        // Create a template object for the original page
                        PdfFormXObject template = page.CopyAsFormXObject(document);
                        // Create a transformation matrix to position and scale the template correctly
                        // Add the template to the new page using the transformation matrix
                        canvas.AddXObject(template);
                    }

                    // Close the reader
                    reader.Close();
                }

                // Update the progress bar
                progress++;
                if (progress % progressInterval == 0)
                {
                    UpdateProgressBar(filePaths.Length);
                }
            }

            // Stop the timer and reset the progress bar
            timer.Stop();
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.WriteLine("[{0}] {1}/{2} files processed", new string('=', 50), progress, filePaths.Length);
            // Close the document
            document.Close();

        }


        static void UpdateProgressBar(int total)
        {
            int progress = 0;
            // Calculate the progress as a percentage
            progress = (int)(((double)progress / total) * 50);

            // Update the progress bar
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("[{0}{1}] {2}/{3} files processed", new string('=', progress), new string(' ', 50 - progress), progress, total);
        }

        static string[] ListPdfFiles(string directoryPath)
        {
            // Get a list of all the PDF files in the directory
            string[] pdfFiles = Directory.GetFiles(directoryPath, "*.pdf");

            // Sort the list of PDF files by name
            Array.Sort(pdfFiles, StringComparer.InvariantCultureIgnoreCase);

            return pdfFiles;
        }
    }
}
